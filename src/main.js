import Vue from 'vue'
import axios from 'axios'
//import filters from './lib/filters'
import iView from 'iview'
import hljs from 'highlight.js'
import 'highlight.js/styles/default.css'
import 'iview/dist/styles/iview.css'
import routerMap from './lib/routers'
import router from './router'
import App from './view/App'
/* eslint-disable no-new */

Vue.config.productionTip = false

Vue.use(iView)

//添加axios
Vue.prototype.$http = axios;


Vue.directive('highlightjs', function(el) {
    let blocks = el.querySelectorAll('pre code');
    Array.prototype.forEach.call(blocks, hljs.highlightBlock);
})
Vue.directive('highlight', function(el, binding) {
    if (binding.value) {
        let oValue = null
        if (typeof(binding.value) === "string") {
            oValue = binding.value
        } else {
            oValue = JSON.stringify(binding.value, null, 4)
        }
        el.innerHTML = '<pre><code class="json hljs">' + hljs.highlight("json", oValue, true).value + '</code></pre>'
    }
})

router.beforeEach((to, from, next) => {
    iView.LoadingBar.start();
    next();
});

router.afterEach((to, from, next) => {
    iView.LoadingBar.finish();
});

new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})