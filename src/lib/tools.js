const formatTag = function(value) {
    if (value.indexOf(" ")) {
        var temp = ''
        value = value.split(" ");
        for (var i = 0; i < value.length; i++) {
            temp = temp + value[i] + '_'
        }
        value = temp.slice(0, temp.length - 1)
    }
    return value;
}
const asArrayToArray = (asArray) => {
    let arr = []
    for (let i in asArray) {
        arr.push(asArray[i])
    }
    return arr
}
const getRealUrl = function(disDocData, requestData) {
    //console.log(disDocData)
    let url = disDocData.path
    if (typeof(disDocData) === 'undefined' || typeof(disDocData.parameters) === 'undefined') {
        return url
    }
    disDocData.parameters.forEach(function(param) {
        if (param.in === 'path') {
            if (typeof(requestData[param.in][param.name]) !== 'undefined') {
                url = url.replace('{' + param.name + '}', requestData[param.in][param.name])
            }
        }
    })
    return url
}
const getQuery = function(data) {
    let queryString = '?'
    for (let query in data['query']) {
        queryString += query + '=' + data['query'][query] + '&'
    }
    return encodeURI(queryString.slice(0, queryString.length - 1))
}
const getForm = function(data) {
    return data.formData
}
const getChildrenList = function(current, all) {
    var list = [];
    var i = 0;
    //console.log(current)
    for (let path in all) {
        for (let method in all[path]) {
            all[path][method].tags.forEach(function(tag) {
                //console.log(tag, current)
                if (tag === current) {
                    all[path][method]['path'] = path
                    all[path][method]['method'] = method
                    list[i++] = all[path][method]
                }
            });
        }
    }
    return list
}
const getFirstChild = function(current, all) {
    return getChildrenList(current, all)[0]
}
const ResponseToRef = function(responses) {
    let resArray = asArrayToArray(responses)
    return resArray[0].schema.$ref || resArray[0].schema.items.$ref
}

const getTag = function(ref) {
    ref = ref.split("/")
    if (ref && ref.length === 3) {
        return ref[2]
    } else {
        return false
    }

}
const refToProperties = function(ref, definitions) {
    //ref = ref.split("/")
    return getTag(ref) ? definitions[getTag(ref)].properties : null
}
const deepCopy = function(p, c) {　　　　
    var c = c || {}　　　　
    for (var i in p) {　　　　　　
        if (typeof p[i] === 'object') {　　　　　　　　
            c[i] = (p[i].constructor === Array) ? [] : {};　　　　　　　　
            deepCopy(p[i], c[i])　　　　　　
        } else {　　　　　　　　　 c[i] = p[i]　　　　　　 }　　　　
    }　　　　
    return c　　
}
const setDescendantProp = function(obj, desc, value) {
    var arr = desc.split('.');
    while (arr.length > 1) {
        obj = obj[arr.shift()];
    }
    obj[arr[0]] = value;
}
const getJson = function(properties, definitions) {
    let arr = {}

    let dataSource = properties
    let keysArr = Object.keys(properties).sort()
    for (let i = 0; i < keysArr.length; i++) {
        if (dataSource[keysArr[i]].type) {
            setDescendantProp(arr, keysArr[i], dataSource[keysArr[i]].type)
                //eval('arr.' + keysArr[i] + '="' + dataSource[keysArr[i]].type + '"')
        } else if (dataSource[keysArr[i]].$ref) {
            let name = keysArr[i]
            let midData = refToProperties(dataSource[keysArr[i]].$ref, definitions)
            setDescendantProp(arr, name, {})
                //eval('arr.' + name + '=' + '{}')
            for (let m in midData) {
                let nameTmp = name + '.' + m
                let keys = name.split('.')
                if (keys[keys.length - 1] === m) {
                    dataSource[nameTmp] = {}
                    dataSource[nameTmp].parentFlag = false
                } else {
                    dataSource[nameTmp] = midData[m]
                    dataSource[nameTmp].parentFlag = true
                }
                keysArr.push(nameTmp)
            }
        }
    }
    return arr
}
export default {
    formatTag,
    getForm,
    getQuery,
    getRealUrl,
    getChildrenList,
    getFirstChild,
    ResponseToRef,
    refToProperties,
    getJson
}