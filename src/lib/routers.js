module.exports = [{
        path: '/',
        name: 'index',
        component: function(resolve) {
            require(['../view/App.vue'], resolve)
        }
    },
    {
        path: '/:id',
        name: 'detail',
        component: function(resolve) {
            require(['../view/App.vue'], resolve)
        }
    }
]