import tools from './tools'
import name_map from './name_map'
const asArrayToArray = (asArray) => {
    let arr = []
    for (let i in asArray) {
        arr.push(asArray[i])
    }
    return arr
}
exports.tagName = function(value) {
    var engName = tools.formatTag(value);
    return name_map[engName];
}
exports.getChildrenList = function(current, all) {
    return tools.getChildrenList(current, all)
}
exports.ResponseToRef = function(responses) {
    let resArray = asArrayToArray(responses)
    return resArray[0].schema.$ref || resArray[0].schema.items.$ref
}
exports.isDefinition = function(responses, definitions) {
    let resArray = asArrayToArray(responses)
    if (typeof(resArray[0]) === 'undefined' || typeof(resArray[0]['schema']) === 'undefined' || typeof(resArray[0]['schema'].items) === 'undefined' ||
        (typeof(resArray[0]['schema'].$ref) === 'undefined' && typeof(resArray[0]['schema'].items.$ref) === 'undefined')
    ) {
        return false
    }
    let ref = resArray[0].schema.$ref || resArray[0].schema.items.$ref
    ref = ref.split("/")

    if (typeof(definitions[ref[2]]) === 'undefined') {
        return false
    }
    return true
}
exports.toUpperCase = function(str) {
    return typeof(str) === 'undefined' || typeof(str.toUpperCase) === 'undefined' ? '' : str.toUpperCase()
}
exports.getCode = function(responses) {
    return Object.keys(responses)[0]
}
exports.refToProperties = function(ref, definitions) {
    //console.log(ref)
    ref = ref.split("/")
    return (ref && ref.length === 3) ? definitions[ref[2]].properties : null
}
exports.formatJson = function(responses, definitions) {
    let ref = tools.ResponseToRef(responses)
    let properties = tools.refToProperties(ref, definitions)
        //console.log('log', ref, properties)

    return tools.getJson(properties, definitions)
}