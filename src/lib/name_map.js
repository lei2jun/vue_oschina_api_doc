export default {
    Activity: '动态通知',
    Gists: '代码片段',
    Git_Data: '仓库数据',
    Issues: 'Issues相关',
    Labels: '标签',
    Milestones: '里程碑',
    Miscellaneous: '杂项',
    Organizations: '组织相关',
    Pull_Requests: 'PR 操作',
    Repositories: '项目仓库',
    Users: '用户帐号',
    Webhooks: '钩子'
}