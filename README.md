# vue2_oschina_api

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

```
修改使用vue2.0创建

##更新
20170528: 更新使用2.0创建，上传第一版。
1. 修复1.0存在递归调用栈溢出问题。
2. 修复1.0对name为‘number’时校验不正确问题。
3. 修复1.0对boolean类型产生错误
4. 使用vue2.0构建

##截图
![运行截图](https://git.oschina.net/uploads/images/2017/0519/141247_69858f00_1025064.jpeg "运行截图")